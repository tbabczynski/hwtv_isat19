function [ T ] = calc_refined_field( tf, im, sigma, cached_btf )
%  CALC_REFINED_FIELD calculate the tensor field after ball voting
%   each voter votes on each other
%
%   T = calc_refined_field( tf, im, sigma, cached_btf )
%
%   tf - voting tensors (n,m,2,2)
%   im - grayscale image
%   sigma - the range of voting parameter
%   cached_btf - OPTIONAL the cached voting field
%   
%   returns T - tensor field after ball voting


    % Get votes for ball
    if nargin < 4
        ball_vf = calc_vote_ball(tf,im,sigma);
    else
        ball_vf = calc_vote_ball(tf,im,sigma, cached_btf);
    end
%    T = ball_vf;
%    return 

%% erase non-voters
% Erase anything that's not in the original image

    [rows, cols] = find(im==0);
    s = size(rows,1);
    
    for i=1:s
        ball_vf(rows(i),cols(i),1,1) = 0;
        ball_vf(rows(i),cols(i),1,2) = 0;
        ball_vf(rows(i),cols(i),2,1) = 0;
        ball_vf(rows(i),cols(i),2,2) = 0;
    end
    
    T = ball_vf;
end
