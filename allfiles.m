DirInfo = dir ('ICDAR2009/*.tif');
DirInfo(cellfun(@(x) isempty(regexp(x,'[0-9]{3}\.tif', 'once')),{DirInfo.name})) =[];

wyniki = zeros(size(DirInfo,1),3);
alltime=tic;
for f=1:size(DirInfo,1)
% for f=65:72
  inputPath = [DirInfo(f).folder, '/', DirInfo(f).name];
  fprintf(1,'%3d %s\n',f,inputPath);
  binImg = readExample(inputPath,0);
%   labImg = TextLineSepTV(binImg,0);
  labImg = TextLineSepHist(binImg,0);
  [o2o_cnt, M, N] = matchscore1(labImg,inputPath);
  wyniki(f,:)=[o2o_cnt, M, N];

% wyniki(f,1)=sum(binImg(:));
% im1=bwmorph(binImg,'skel',Inf);
% wyniki(f,2)=sum(im1(:));
% im1=bwmorph(binImg,'remove',Inf);
% wyniki(f,3)=sum(im1(:));

end

wynik = sum(wyniki);
statystyki = [wyniki(:,1)./wyniki(:,2), wyniki(:,1)./wyniki(:,3)];
statystyka = [wynik(1)./wynik(2), wynik(1)./wynik(3)];

toc(alltime);