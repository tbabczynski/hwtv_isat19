function [labImg] = TextLineSepTV(binImg,dbgLev)
  %TEXTLINESEPHIST Labeling of lines using histograms
  %   Function labels lines of text in binary image using the adaptive
  %   horizontal histograms
  %
  % input - binImg - binary image with 0s as background
  % dbgLev - level of
  % debugging (showing the generated images at the stages of algorithm)
  %
  % output - labImg - labeled image (uint8)
  
  if nargin<2
    dbgLev = 0;
  end
  creategif=false;
  
  %% start algorytmu
  % wejscie - binImg - binarny obraz 0 tlo, 1 obraz
  
  AH = average_height(binImg);
  
  % sigma=2*AH; %\sigma of tensor voting
  sigma=100; %\sigma of tensor voting
  strokeW=6; %width of the pen
% 	skIm = bwskel(binImg);
% 	strokeW = 1+round(sum(binImg(:))/sum(skIm(:)));
  charW=AH; %width of the character
  lineDistance=uint16(round(AH/2)); %distance between cutting lines
  
  se=strel('line',charW,0);
  %     se2=strel('rectangle',[strokeW, charW+strokeW]);
  se2=strel('line',strokeW+charW,0);
  
  
  %cutImg=imclose(binImg,se);
  cutImg=imdilate(binImg,se);
  cutImg=imerode(cutImg,se2);
  %cutImg=binImg;
  
  cutImg(:,lineDistance:lineDistance:end)=0; % :D
  
  if dbgLev > 0
    debugsubplot(2,3,3); imshow(cutImg);
    title('closed and cut image'); drawnow();
  end
  
  %% centroid extraction
  
  boolImg = logical(cutImg);
  % stat = clean_stats(regionprops(boolImg,'centroid','area','BoundingBox'));
  stat = regionprops(boolImg,'centroid','area','BoundingBox');
  
  centroids = cat(1,stat.Centroid);
  areaMax = max([stat.Area]);
  
  centrImg=zeros(size(boolImg,1),size(boolImg,2));
  for i=1:size(stat)
    area = stat(i).Area;
    val = area/areaMax;
    centrImg(int16(stat(i).Centroid(2)), int16(stat(i).Centroid(1)))=val;
    
    %   if stat(i).BoundingBox(4)>AH
    %     centrImg(int16(stat(i).Centroid(2)+AH/2), int16(stat(i).Centroid(1)))=val/2;
    %       if stat(i).Centroid(2)>AH/2
    %           centrImg(int16(stat(i).Centroid(2)-AH/2), int16(stat(i).Centroid(1)))=val/2;
    %       end
    %   end
  end
  
  if dbgLev > 0
    %imwrite(centrImg,'centroids_noClose.png');
    debugsubplot(2,3,4);
    %     imshow(centrImg);
    imshow(img);hold on; plot(centroids(:,1),centroids(:,2),'r+');hold off;
    title('extracted centroids'); drawnow();
  end
  
  %% initial field prepare
  
  sparse_tf = calc_initial_field(centrImg);
  
  if dbgLev > 1
    debugsubplot(2,3,5); show_tensorfield(sparse_tf,true,true,20);
    title('initial field'); drawnow();
  end
  
  if creategif
    figure;
    pause;
    [~, ~, L1, L2] = convert_tensor_ev(sparse_tf);
    imshow(L1-L2);
    %     show_tensorfield(sparse_tf,false,true,20);
    [imind,cm] = rgb2ind(frame2im(getframe),256);
    imwrite(imind,cm,'deb.gif','gif', 'Loopcount',inf);
  end
  
  %% sparse voting
  
  tic
  % voted1_tf = calc_refined_field_full(sparse_tf, centrImg, sigma, cached_vtf);
  % [voted1_tf, voted1Max_tf] = calc_refined_field_full(sparse_tf, centrImg, sigma);
  % voted1_tf = calc_refined_field_full(sparse_tf, centrImg, sigma);
  voted1_tf = calc_refined_field_steerable(sparse_tf, centrImg, sigma);
  toc
  if dbgLev > 1
    debugsubplot(2,3,6); show_tensorfield(voted1_tf,true,true,20);
    title('refined field after sparse v.'); drawnow();
  end
  
  if creategif
    [~, ~, L1, L2] = convert_tensor_ev(voted1_tf);
    imshow(L1-L2);
    %     show_tensorfield(voted1_tf,false,true,20);
    [imind,cm] = rgb2ind(frame2im(getframe),256);
    imwrite(imind,cm,'deb.gif','gif', 'WriteMode','append');
  end
  
  %% outsiders removing 1
  
  removed1_tf = remove_outsiders(voted1_tf, AH);
  % removed1Max_tf = remove_outsiders(voted1Max_tf);
  
  if dbgLev > 1
    debugsubplot(2,3,6); show_tensorfield(removed1_tf,true,true,20);
    title('refined field after removing'); drawnow();
  end
  
  %% find lines
  
  tic
  P = pointsFromTF(removed1_tf);
  % P = logical(centrImg);
  [linestab, linNums] = lineFromPoints(P,AH,sigma,~binImg);
  toc
  
  %% label picture
  tic
  % labImg = labelsFromLines(linestab, binImg);
  labImg = labelsFromLines_v2Chineese(linestab, linNums, binImg);
  toc
  
end

%% functions

function stat = clean_stats(stat)
  areas = cat(1,stat.Area);
  areaLo = prctile(areas,5);
  %     areaHi = prctile(areas,99);
  areaHi = max(areas);
  
  stat = stat(areas > areaLo & areas < areaHi);
  
end

function ah = average_height(img)
  if ~islogical(img)
    error('image should be binary but is %s', class(img));
  end
  s = regionprops(img, 'BoundingBox');
  bb = cat(1,s.BoundingBox);
  ah = round(mean(bb(:,4)));
end

function P = pointsFromTF(TF,eps)
  if nargin <2
    eps = 1e-5;
  end
  [~,~,L1,L2]= convert_tensor_ev(TF);
  P = (L1-L2)>eps;
end

