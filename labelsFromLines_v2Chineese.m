function [labels] = labelsFromLines_v2Chineese(linestab, linNums, img)
%LABELSFROMLINES Summary of this function goes here
%   Detailed explanation goes here

%Wyznaczenie punktów krzywej łamanej
%DD - macierz przechowuje współrzędne y punktów stanowiących krzywe łamane
%pomiędzy punktami 'lines'
%j - zlicza kolejne wyznaczone krzywe łamane
DD = cell(size(linestab));
imgLines = zeros(size(img),'uint8');
labels = zeros(size(img),'uint8');
for j=1:size(linestab,2)
	for i=1:size(linestab{j},1)-1
		coef = polyfit([linestab{j}(i,1), linestab{j}(i+1,1)], [linestab{j}(i,2), linestab{j}(i+1,2)], 1);
		y = round(polyval(coef,linestab{j}(i,1):linestab{j}(i+1,1)));
    y(y<1) = 1; %czasem z zaokrąglenia wychodzą piksel poza obrazkiem
    y(y>size(img,1))=size(img,1);
    if i==1
      DD{j}=[y; linestab{j}(i,1):linestab{j}(i+1,1)];
    else
      DD{j}=[DD{j}(1,1:end-1), y;...
        DD{j}(2,1:end-1), linestab{j}(i,1):linestab{j}(i+1,1)];
    end
	end
    imgLines(sub2ind(size(img),DD{j}(1,:),DD{j}(2,:)))=linNums(j);
% 	plot(1:CC{j}(end,1),DD{j})
% 	hold on
end

regions = regionprops(img,'BoundingBox','Image');

for r=1:size(regions)
  low = flip(ceil(regions(r).BoundingBox(1:2)));
  high = low+flip(regions(r).BoundingBox(3:4))-1;
  bb = imgLines(low(1):high(1),low(2):high(2));
  bbpos = unique(bb(bb>0));
  if isempty(bbpos) % żadna linia nie przecina
    margin=0;
    while isempty(bbpos) && margin<size(img,1)
      margin = margin+10;
      bbM = imgLines(max(1,low(1)-margin):min(size(img,1),high(1)+margin),...
        max(1,low(2)-margin):min(size(img,2),high(2)+margin));
      bbpos = unique(bbM(bbM>0));
    end
    if isempty(bbpos); continue; % brzydkie obejście! No ale jak nie znaleziono...
    elseif size(bbpos,1)==1
      labels(low(1):high(1),low(2):high(2)) = ...
        labels(low(1):high(1),low(2):high(2)) + uint8(regions(r).Image)*bbpos;
    else
      bbi= zeros(size(bb),'uint8');
      [vy, vx]=find(bbM);
      vyM=vy-low(1)+max(1,low(1)-margin);
      vxM=vx-low(2)+max(1,low(2)-margin);
      [vyI,vxI]=find(regions(r).Image);
      vyI = vyI(:);
      vxI = vxI(:);
      [~,dI] = min(hypot(repmat(vyM,[1, size(vyI,1)])-repmat(vyI',[size(vyM,1), 1]),...
        repmat(vxM,[1, size(vxI,1)])-repmat(vxI',[size(vxM,1),1])));
      bbi(sub2ind(size(bbi),vyI,vxI)) = bbM(sub2ind(size(bbM),vy(dI),vx(dI)));
      
      labels(low(1):high(1),low(2):high(2)) = ...
        labels(low(1):high(1),low(2):high(2)) + bbi;
    end
  elseif size(bbpos,1)==1 % jedna linia - cały region do tej linii
    labels(low(1):high(1),low(2):high(2)) = ...
      labels(low(1):high(1),low(2):high(2)) + uint8(regions(r).Image)*bbpos;
  else % więcej niż jedna linia
    bbi= zeros(size(bb),'uint8');
    [vy, vx]=find(bb);
    [vyI,vxI]=find(regions(r).Image);
%wersja z pętlą, poniżej zwektoryzowana - podobnie szybkie
%     for iI=1:size(vyI) 
%       [~,diI] = min(hypot(vy-vyI(iI), vx-vxI(iI)));
%       bbi(vyI(iI),vxI(iI))=bb(vy(diI),vx(diI));
%     end
    [~,dI] = min(hypot(repmat(vy,[1, size(vyI,1)])-repmat(vyI',[size(vy,1), 1]),...
      repmat(vx,[1, size(vxI,1)])-repmat(vxI',[size(vx,1),1])));
    bbi(sub2ind(size(bbi),vyI,vxI)) = bb(sub2ind(size(bbi),vy(dI),vx(dI)));

    labels(low(1):high(1),low(2):high(2)) = ...
      labels(low(1):high(1),low(2):high(2)) + bbi;
  end
end


end

