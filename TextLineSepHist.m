function [labImg, borders] = TextLineSepHist(binImg,dbgLev)
	%TEXTLINESEPHIST Labeling of lines using histograms
	%   Function labels lines of text in binary image using the adaptive
	%   horizontal histograms
	%
	% input - binImg - binary image with 0s as background
	% dbgLev - level of
	% debugging (showing the generated images at the stages of algorithm)
	%
	% output - labImg - labeled image (uint8)
	% borders - coordinates of consecutive borders between lines of text
	
	if nargin<2
		dbgLev = 0;
	end
	
	%% Obliczenie rzutu poziomego
	rzut=sum(binImg,2);
	if dbgLev>0
		figure;
		plot(rzut);
		view([-90,-90]);
	end
	
	%% Wygładzanie rzutu
	n=size(rzut,1);
	rr = int32(n/300);
	s2 = [zeros(rr, 1); rzut; zeros(rr, 1)];
	rzut = zeros(size(s2));
	for i=(rr+1):(n-rr)
		rzut(i) = mean(s2((i-rr):(i+rr)));
	end
	rzut = rzut((rr+1):(rr+n));
	n = int32(n);
	
%   ffshrzut=fftshift(fft(rzut));
%   ffshrzut(abs(ffshrzut)<max(abs(ffshrzut))/50)=0;
%   rzut=ifft(ifftshift(ffshrzut));
  
	if dbgLev>0
		figure
		plot(rzut, 'black'), axis([0, n, 0, max(rzut)]);
		view([-90, -90]);
		%title([progIn sr]);
	end
	
	flagi = zeros(1, n);
	linestab = {};
	[maksima, indeksy] = sort(rzut, 'descend');
	
	%-----wykres na obrazku
	mx = max(rzut);
	wykr = zeros(n, int32(mx*10/9));
	
	for i=1:n
		for j=1:int32(rzut(i)+1)
			wykr(i, j) = 1;
		end
	end
	%--------------------
	j=1;
	
	for i=1:n
		mx = maksima(i);
		ind = indeksy(i);
		
		if mx <= maksima(1)/10
			break;
		end
		
		if ind == 1 || ind == n
			continue;
		end
		
		if rzut(ind-1) > rzut(ind) || rzut(ind+1) > rzut(ind)
			continue;
		end
		%%%
		progIn=0.5;
		prog = progIn*mx;
		
		%Jesli wierzcholek nie zawiera sie w wyznaczonych wczesniej odcinkach
		if flagi(ind) == 0
			
			iL = ind; %indeksy lewy i prawy
			iP = ind;
			gL = 0;   %granice lewa i prawa
			gP = 0;
			
			%WYZNACZANIE SZEROKOSCI WZNIESIEN HISTOGRAMU
			while gL == 0 || gP == 0
				
				if gL == 0; iL = iL - 1; end
				if gP == 0; iP = iP + 1; end
				if iL == 0 || iP == n; break; end
				
				%Jesli odcinek pokrywa sie z juz innym, wczesniej dodanym do
				%tablicy flagi[]
				if flagi(iL) == 1 || flagi(iP) == 1
					gL = 0;
					break;
				end
				
				if gL == 0 && rzut(iL) < prog
					gL = iL;
				end
				
				if gP == 0 && rzut(iP) < prog
					gP = iP;
				end
			end
			
			if gL ~= 0
				flagi(gL:gP) = 1;
				linestab{j} = [1,floor(gL+gP)/2;size(binImg,2),floor(gL+gP)/2]; %#ok<AGROW>
				j=j+1;
				if dbgLev>0
					hold on
					plot(gL:gP, prog*ones(1, gP-gL+1), 'black');
					view([-90, -90]);
				end
				
				wykr(gL:gP, int32(prog):int32(prog+7)) = 0;
			end
		end
	end
	
	if dbgLev>0
		figure
		plot(flagi, 'black'), axis([0, n, 0, 2]);
	end
	
	%% WYZNACZANIE GRANIC MIEDZY ZNALEZIONYMI ODCINKAMI
	borders = zeros(1, n);
	nGranic = 0;
	minimum = maksima(1)+1;
	iMin = 0;
	szer = -1;
	for i=1:n
		if flagi(i) == 0    % szukamy minimum
			if rzut(i) < minimum
				minimum = rzut(i);
				iMin = i;
				szer = 0;
				
			elseif rzut(i) == minimum
				szer = szer + 1;
			end
		elseif szer > -1
			nGranic = nGranic + 1;
			borders(nGranic) = iMin + szer/2;
			minimum = maksima(1)+1;
			szer = -1;
			
			wykr((iMin + szer/2):(iMin + szer/2)+3,:)=0;
		end
		
	end
	
	if dbgLev>0
		imtool(wykr);
	end
	
	nGranic = nGranic + 1;
	borders(1) = 1;
	borders(nGranic) = n;
	
	%% ZAMIANA GRANIC NA PRZEDZIALY
	granice2 = borders(1:nGranic);
	borders = zeros(nGranic - 1, 2);
	for i=1:(nGranic-1)
		borders(i, 1:2) = [granice2(i) + 1, granice2(i+1)];
	end
	borders(1, 1) = 1;
	%   nGranic = nGranic - 1;
	
% 	labImg = bordersToLabels(binImg, borders);
	linNums=1:j-1;
	labImg = labelsFromLines_v2Chineese(linestab, linNums, binImg);
	
end

%% funkcje pomocnicze


function labImg = bordersToLabels(binImg, granice)
	labImg = zeros(size(binImg),'uint8');
	for i=1:size(granice,1)
		labImg(granice(i,1):granice(i,2),:)=i;
	end
	labImg(~binImg)=0;
end

