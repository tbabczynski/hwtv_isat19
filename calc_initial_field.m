function [T] = calc_initial_field(image)
%%CALC_INITIAL_FIELD Create starting tensor field
%   put ball tensor on each position where 
%   original image has something in foreground (>0)
%
%   T = calc_initial_field(image)
%
%   image - grayscale image
%
%   returns a tensor field T
%

    [h, w] = size(image);
    T = zeros(h,w,2,2);
    
    [rows,cols] = find(image>0);
    
%    immin=min(image(:));
%    immax=max(image(:));
    
    n = size(rows,1);
    for i=1:n
        r = rows(i);
        c = cols(i);
%         v = image(r,c);
        v = 1;
%         T(r,c,:,:) = [v,0;0,v]; %ball tensor
        T(r,c,:,:) = [0,0;0,v]; %stick vertical tensor
%         T(r,c,:,:) = [v,0;0,0]; %stick horizontal tensor
    end
end

