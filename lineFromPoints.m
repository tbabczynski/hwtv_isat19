function [linestab, linNums] = lineFromPoints(initialPoints, AH, sigma, im)
%LINEFROMPOINTS generates array of lines from series of points
%   Detailed explanation goes here


i=0;
linestab = {};
while(true)
	P=ZnajdzPoczatkowy(initialPoints);
	if isempty(P)
		break
	end
	punkty=P;
	while P(1) < size(initialPoints,2)
		[initialPoints, Q]=NastepnyPunkt(initialPoints, P, sigma, AH);
		if isempty(Q)
			break
		end
		punkty=[punkty; Q]; %#ok<AGROW>
		P=Q;
	end
	%Dodajemy tylko łamane o odpowiedniej długości
	if  size(punkty,1)>1 && punkty(end,1)-punkty(1,1) > AH

      i=i+1;
      linestab{i}=punkty; %#ok<AGROW>
      %Dodanie punktów skrajnych
%       lines{i}=unique(...
%         [1, lines{i}(1,2);...
%         lines{i};...
%         size(initialPoints,2), lines{i}(end,2)],...
%         'rows'); %#ok<AGROW>
      linestab{i}=unique(...
        [max(1,linestab{i}(1,1)-sigma), linestab{i}(1,2);...
        linestab{i};...
        min(size(initialPoints,2),linestab{i}(end,1)+sigma), linestab{i}(end,2)],...
        'rows'); %#ok<AGROW>

	else
		initialPoints(P(2),P(1))=0;
	end
end

mmm = cellfun(@(x) mean(x(:,2)), linestab); %sortowanie linii
[~,mmmo]=sort(mmm);
linestab = linestab(mmmo);

[linestab, linNums] = remove_redundant_lines(linestab,im, AH);

% imshow(im);
% hold on
% kol = lines;
% cellfun(@(x,y) plot(x(:,1),x(:,2), '-o', 'Color',kol(y,:)),linestab, num2cell(linNums));
% figure
% imshow(im);
% hold on
% cellfun(@(x) plot(x(:,1),x(:,2), '-o'),lines1);

end


%% funkcje pomocnicze


function [P] = ZnajdzPoczatkowy(obraz_we)
%ZNAJDZPOCZATKOWY Funkcja znajduje początkowy punkt w obrazie licząc od
%lewej do prawej i z góry na dół
  x_pocz=find(sum(obraz_we(:,1:uint16(size(obraz_we,2)*(2/2)))));
  if isempty(x_pocz)
    P=[];
    return
  end
  x_pocz=x_pocz(1);
  y_pocz=find(obraz_we(:,x_pocz));
  y_pocz=y_pocz(1);
  P=[x_pocz, y_pocz];
end

function [obraz_we, Q] = NastepnyPunkt(obraz_we, P, sigma, AH)

okno_xp=P(1);
okno_xk=min(P(1)+2*sigma-1, size(obraz_we,2));
okno_yp=max(P(2)-AH, 1);
okno_yk=min(P(2)+AH, size(obraz_we,1));
okno=obraz_we(okno_yp:okno_yk , okno_xp+1:okno_xk);
[y, x]=find(okno);
odleglosci=abs(y-min([AH+1, P(2)])); % nie rozumiem tego "min"
min_odleglosc=min(odleglosci);
idx = find(odleglosci == min_odleglosc);
najblizsze=[x(idx), y(idx)];
%P
%P_local = [1, max([AH+1, 1])];
if isempty(najblizsze) 
  Q=[];
  obraz_we(P(2),P(1))=0;
  return
end    
%Wybieramy punkt najdalszy we współrzędnej x
% Q_local=najblizsze(end,:);
%Wybieramy punkt najbliższy we współrzędnej x
Q_local=najblizsze(1,:);
%Przeliczanie współrzędnych punktu Q z układu lokalnego na globalny obrazu
Q = Q_local+[okno_xp,okno_yp];
% if P(2)>AH
%   Q=[Q_local(1)+P(1), Q_local(2)+P(2)-AH-1];
% else
%   Q=[Q_local(1)+P(1), Q_local(2)+P(2)-AH];
% end
%Zerowanie przejrzanych punktów
obraz_we(okno_yp:okno_yk , okno_xp:Q(1))=0;

end

% function [lines, found] = correctTooClose(lines,punkty,TooClose)
%   found = false;
%   for i=1:size(lines,2)
%     if abs(mean(lines{i}(:,2))-mean(punkty(:,2)))< TooClose
%       lines{i} = [lines{i}; punkty];
%       lines{i} = sortrows(lines{i});
%       lines{i}(1,2) = lines{i}(2,2);
%       lines{i}(end,2) = lines{i}(end-1,2);
%       lines{i} = unique(lines{i},'rows');
%       found = true;
%       return;
%     end
%   end
% end

function [linestab, linNums] = remove_redundant_lines(linestab,im, AH)
%REMOVE_REDUNDANT_LINES removes lines pointing only to already pointed
%regions

  % 	imshow(im);
	stats = regionprops(~im, 'BoundingBox');
	bboxes = cat(1,stats.BoundingBox);
  
	ind = bboxes(:,4)<AH/2;
	bboxes(ind,:)=[];
	M = zeros(size(linestab,2),size(bboxes,1),'logical');
	for bbind=1:size(bboxes,1)
		bb=bboxes(bbind,:);
		xlimit = [ceil(bb(1)), floor(bb(1))+bb(3)];
		ylimit = [ceil(bb(2)), floor(bb(2))+bb(4)];
		xbox = xlimit([1 1 2 2]);
		ybox = ylimit([1 2 2 1]);
		poly = polyshape(xbox,ybox);
		for llind=1:size(linestab,2)
			ll=linestab{llind};
			if max(ylimit)<min(ll(:,2))
				continue;
			elseif min(ylimit)>max(ll(:,2))
				continue;
			end
			[in,~] = intersect(poly,ll);
			if~isempty(in)
				M(llind,bbind) = true;
% 				hold on; plot(poly);
			end
		end
	end
	m1 = sum(M,1);
% 	m2 = sum(M,2);
	Mnz=M(:,m1>0);
	m1(~m1)=[];
% 	m1LOI = find(m1>1);
	ind = zeros(size(linestab),'logical');
	for llind=1:size(linestab,2)
% 		if min(m1(Mnz(llind,:)))>1
		if size(find(m1(Mnz(llind,:))==1),2)<=1 
      % dopuszczamy jeden znak nienależący do innej linii
			ind(llind)=true;
		end
	end
	linestab(ind)=[];
  
  %% glue lines

  linNums = 1:size(linestab,2);
  for linInd=1:size(linestab,2)-1
    s1 = linestab{linInd};
    s2 = linestab{linInd+1}; %linie są posortowane w y
    % 2, end-1 - bo są dodane sztucznie końce
		if (abs(s1(2,2)-s2(end-1,2))< AH && ... % close in y dir (orig)
				abs(s1(2,1)-s2(end-1,1))<size(im,2)/2) || ... % close in x dir
				(abs(s2(2,2)-s1(end-1,2))< AH && ...
				abs(s2(2,1)-s1(end-1,1))<size(im,2)/2)
			linNums(linInd+1:end) = linNums(linInd+1:end)-1;
		end
  end

end