%clear all
figure
%Parametry
%AH=2; sigma=3;
AH=20; sigma=100;

load("o_pozycje.mat");
%input_image=zeros(7, 15);
input_image=P2; %Pe;
processed_image=input_image;

%imtool("/home/staff/faster/MATLAB/hwtv_isat19/ICDAR2013/005_1a.tif")
imshow("/home/staff/faster/MATLAB/hwtv_isat19/ICDAR2013/005_1a.tif")

i=0;
while(true)
  P=ZnajdzPoczatkowy(processed_image);
  if isempty(P)
    break
  end
  punkty=P;
  while P(1) < size(processed_image,2)
    [processed_image, Q]=NastepnyPunkt(processed_image, P, sigma, AH);
    if isempty(Q)
      break
    end
    punkty=[punkty; Q];
    P=Q;
  end
  %Dodajemy tylko łamane o odpowiedniej długości
  if  size(punkty,1)>1 & punkty(end,1)-punkty(1,1) > sigma
    i=i+1;
    CC{i}=punkty;
  end
  %Dodanie punktów skrajnych
  CC{i}=[1, CC{i}(1,2); CC{i}; size(processed_image,2), CC{i}(end,2)];
  hold on
  plot(CC{i}(:,1),CC{i}(:,2)) %, 'x')
end

%% funkcje


%Funkcja znajduje początkowy punkt w obrazie licząc od lewej do prawej i z
%góry na dół
function [P] = ZnajdzPoczatkowy(obraz_we)
  x_pocz=find(sum(obraz_we));
  if isempty(x_pocz)
    P=[];
    return
  end
  x_pocz=x_pocz(1);
  y_pocz=find(obraz_we(:,x_pocz));
  y_pocz=y_pocz(1);
  P=[x_pocz, y_pocz];
end

function [obraz_we, Q] = NastepnyPunkt(obraz_we, P, sigma, AH)

okno_xp=P(1);
okno_xk=min([P(1)+2*sigma-1], size(obraz_we,2));
okno_yp=max([P(2)-AH, 1]);
okno_yk=min([P(2)+AH, size(obraz_we,1)]);
okno=obraz_we(okno_yp:okno_yk , okno_xp+1:okno_xk);
[y x]=find(okno);
odleglosci=abs(y-min([AH+1, P(2)]));
min_odleglosc=min(odleglosci);
idx = find(odleglosci == min_odleglosc);
najblizsze=[x(idx), y(idx)];
%P
%P_local = [1, max([AH+1, 1])];
%Wybieramy punkt najdalszy we współrzędnej x
if isempty(najblizsze) 
  Q=[];
  obraz_we(P(2),P(1))=0;
  return
end    
Q_local=najblizsze(end,:);
%Przeliczanie współrzędnych punktu Q z układu lokalnego na globalny obrazu
if P(2)>AH
  Q=[Q_local(1)+P(1), Q_local(2)+P(2)-AH-1];
else
  Q=[Q_local(1)+P(1), Q_local(2)+P(2)-AH];
end
%Zerowanie przejrzanych punktów
obraz_we(okno_yp:okno_yk , okno_xp:Q(1))=0;

end