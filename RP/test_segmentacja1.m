%% wczytanie
% clear
% close all
debug=1;
image=imread('inputImg/dokument007.jpg');
if debug>0
  imshow(image)
end
%obraz=TextLineSeparation(i, 0.1, 50);

%% Zamiana na odcienie szarości obrazu image
gray=image(:,:,1)*0.2989+image(:,:,2)*0.5870+image(:,:,3)*0.1140;
if debug>0
  figure
  imshow(gray)
end
%% Binaryzacja obrazu image
Thresh = graythresh(gray);
BW = imbinarize(gray,Thresh);
if debug>0
  figure;
  imshow(BW);
end
%Odszumianie otwarciem morfologicznym
% se = strel('disk',5);
% afterOpening = imopen(BW,se);
% afterClosing = imclose(afterOpening,se);
% if debug>0
%   figure
%   imshow(afterOpening,[]);
%   imshow(afterClosing,[]);
% end

%% Obliczenie rzutu poziomego
rzut=sum(1-BW,2);
if debug>0
  figure;
  plot(rzut);
  view([-90,-90]);
end

%% Wygładzanie rzutu
n=size(rzut,1);
rr = int32(n/300);
s2 = [zeros(rr, 1); rzut; zeros(rr, 1)];
rzut = zeros(size(s2));
for i=(rr+1):(n-rr)
  rzut(i) = mean(s2((i-rr):(i+rr)));
end
rzut = rzut((rr+1):(rr+n));
n = int32(n);

if debug>0
  figure
  plot(rzut, 'black'), axis([0, n, 0, max(rzut)]);
  view([-90, -90]);
  %title([progIn sr]);
end

flagi = zeros(1, n);
[maksima indeksy] = sort(rzut, 'descend');

%-----wykres na obrazku
mx = max(rzut);
wykr = zeros(n, int32(mx*10/9));

for i=1:n
  for j=1:int32(rzut(i)+1)
    wykr(i, j) = 1;
  end
end
%--------------------

for i=1:n
  mx = maksima(i);
  ind = indeksy(i);
  
  if mx <= maksima(1)/10
    break;
  end
  
  if ind == 1 || ind == n
    continue;
  end
  
  if rzut(ind-1) > rzut(ind) || rzut(ind+1) > rzut(ind)
    continue;
  end
  %%%
  progIn=0.5;
  prog = progIn*mx;
  
  %Jesli wierzcholek nie zawiera sie w wyznaczonych wczesniej odcinkach
  if flagi(ind) == 0
    
    iL = ind; %indeksy lewy i prawy
    iP = ind;
    gL = 0;   %granice lewa i prawa
    gP = 0;
    
    %WYZNACZANIE SZEROKOSCI WZNIESIEN HISTOGRAMU
    while gL == 0 || gP == 0
      
      if gL == 0; iL = iL - 1; end
      if gP == 0; iP = iP + 1; end
      
      %Jesli odcinek pokrywa sie z juz innym, wczesniej dodanym do
      %tablicy flagi[]
      if flagi(iL) == 1 || flagi(iP) == 1
        gL = 0;
        break;
      end
      
      if gL == 0 && rzut(iL) < prog
        gL = iL;
      end
      
      if gP == 0 && rzut(iP) < prog
        gP = iP;
      end
    end
    
    if gL ~= 0
      flagi(gL:gP) = 1;
      if debug>0
        hold on
        plot(gL:gP, prog*ones(1, gP-gL+1), 'black');
        view([-90, -90]);
      end
      
      wykr(gL:gP, int32(prog):int32(prog+7)) = 0;
    end
  end
end

if debug>0
  figure
  plot(flagi, 'black'), axis([0, n, 0, 2]);
end

granice = zeros(1, n);
nGranic = 0;
min = maksima(1)+1;
iMin = 0;
szer = -1;

%% WYZNACZANIE GRANIC MIEDZY ZNALEZIONYMI ODCINKAMI
for i=1:n
  if flagi(i) == 0    % szukamy minimum
    if rzut(i) < min
      min = rzut(i);
      iMin = i;
      szer = 0;
      
    else if rzut(i) == min
        szer = szer + 1;
      end
    end
  else if szer > -1
      nGranic = nGranic + 1;
      granice(nGranic) = iMin + szer/2;
      min = maksima(1)+1;
      szer = -1;
    end
  end
  
end

nGranic = nGranic + 1;
granice(1) = 1;
granice(nGranic) = n;

%% ZAMIANA GRANIC NA PRZEDZIALY
granice2 = granice(1:nGranic);
granice = zeros(nGranic - 1, 2);
for i=1:(nGranic-1)
  granice(i, 1:2) = [granice2(i) + 1, granice2(i+1)];
end
granice(1, 1) = 1;
nGranic = nGranic - 1;

if debug>0
  %Wyswietlenie podzielonego obrazu
  WyswietleniePodzielonegoObrazuBW(BW, granice)
  WyswietleniePodzielonegoObrazuKolor(image, granice, 9)
end


%% ----- Funkcje -----

function WyswietleniePodzielonegoObrazuBW(obraz_we, granice, grubosc)
  
  if (nargin < 3)
    grubosc=8;
  end
  
  nGranic=size(granice,1)+1;
  for i=1:nGranic
    obraz_we(granice(i):(granice(i)+grubosc-1), :) = 0;
  end
  figure
  imshow(obraz_we)
  
end


function WyswietleniePodzielonegoObrazuKolor(obraz_we, granice, grubosc)
  
  if (nargin < 3)
    grubosc=8;
  end
  
  nGranic=size(granice,1)+1;
  for i=1:nGranic
    obraz_we(granice(i):(granice(i)+grubosc-1), :, :) = 0;
    obraz_we(granice(i):(granice(i)+grubosc-1), :, 1) = 255;
  end
  figure
  imshow(obraz_we)
  
end