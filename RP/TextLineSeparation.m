function [  image_out] = TextLineSeparation( image_in, threshold, window_size)

% The algorithm has three input parameters:
% 1)image_in - a text lines image,
% 2)threshold - the relative threshold
% 3)window_size - the window of smoothing filter
% 
% The algorithm has one output:
% 1)image_ out - the image with text line separators marked on it
% 
% Additionally a figure of the separated image and its projection profile is showed during the algorithm execution





%"translation" of variable names from polish to english
S = image_in;
progZew = threshold;
sr = window_size;


% A - tutaj to jest BW
A = ReadToBinaryA(S);



prog = progZew;   %bylo 0.825   
[granice, n, ObrazZLiniami] = wyznaczGranice(A, prog, sr, S);
% 
% 
% disp('   Przyporz�dkowanie fragment�w do linii');
% [linieOut startPoints] = oddzielLinie(S, granice);
linieOut = 1;
startPoints = 2;

image_out = ObrazZLiniami;
end

%% pomocnicze funkcje


function [linieOut, startPoints]= oddzielLinie(S, granice)

[n, m] = size(S);
[nG, mG] = size(granice);
[flagi, nObsz]= bwlabel(S);

linieOut(1:nG) = libpointer('int32Ptr', []);
startPoints = zeros(1, nG);

nowaGranica = granice(1, 1);

for i=1:(nG-1)
    
    g1a = nowaGranica;
    g1b = granice(i, 2);
    g2a = granice(i+1,1);
    g2b = granice(i+1, 2);
    
    %wstepne linie jako puste obrazy w rozmiarach calego szkieletu
    linia1 = zeros(size(S));
    linia2 = zeros(size(S));
    
    %skopiowanie wlasciwych linii
    linia1(g1a : g1b, : ) = S(g1a : g1b, : );
    linia2(g2a : g2b, : ) = S(g2a : g2b, : );
    
    Obszary = zeros(1, nObsz);
 
    for j=1:m
        if S(g1b, j) == 1 
            flaga = flagi(g1b, j);
            
            if Obszary(flaga)
                continue;
            end
            Obszary(flaga) = 1;           
            
            %Znalezienie czesci fragmentu nad i pod granica
            L1 = flagi(g1a : g1b, :) == flaga;
            L2 = flagi(g2a : g2b, :) == flaga;
            
            sum1 = sum(L1(:));
            sum2 = sum(L2(:));
            
            %Przeniesienie czesci wiekszej do czesci mniejszej
            if sum1 > sum2
                linia1(g2a : g2b, : ) = linia1(g2a : g2b, : ) | L2;
                linia2(g2a : g2b, : ) = linia2(g2a : g2b, : ) & ~L2;                
            else
                linia2(g1a : g1b, : ) = linia2(g1a : g1b, : ) | L1;
                linia1(g1a : g1b, : ) = linia1(g1a : g1b, : ) & ~L1;
            end                       
        end        
    end
   
    koniecLinii = find(sum(linia1, 2), 1, 'last') + 10;    
    linieOut(i) = libpointer('int32Ptr', linia1(g1a:koniecLinii, :));
    startPoints(i) = nowaGranica-1;
    
    nowaGranica = find(sum(linia2, 2), 1, 'first') - 10;
    S = S & ~linia1;
end

linieOut(nG) = libpointer('int32Ptr', linia2(nowaGranica : granice(nG, 2), :));
startPoints(nG) = nowaGranica-1;

end %function




function [granice, nGranic, ObrazZLiniami] = wyznaczGranice(A, progIn, sr, Ako)

s = sum(A, 2);
[n, n2] = size(s);

%U�REDNIENIE HISTOGRAMU
rr = int32(sr/2); %int32(n/300);      %bylo 100
s2 = [zeros(rr, 1); s; zeros(rr, 1)];
s = zeros(size(s2));
for i=(rr+1):(n-rr)
    s(i) = mean(s2((i-rr):(i+rr)));
end
s = s((rr+1):(rr+n));
n = int32(n);

% figure(2); 
clf;
subplot(2, 2, 2);
plot(s, 'black'), axis([0, n, 0, max(s)]);
title([progIn sr]);


flagi = zeros(1, n);
[maksima, indeksy] = sort(s, 'descend');


%-----wykres na obrazku
mx = max(s);
wykr = zeros(n, int32(mx*10/9));

for i=1:n
    for j=1:int32(s(i)+1)
        wykr(i, j) = 1;
    end
end
%--------------------


for i=1:n
    mx = maksima(i);   
    ind = indeksy(i);   
    
    if mx <= maksima(1)/10
        break;
    end
    
    if ind == 1 || ind == n
        continue;
    end
    
    if s(ind-1) > s(ind) || s(ind+1) > s(ind)
        continue;
    end
    
    prog = progIn*mx;
    
    %Jesli wierzcholek nie zawiera sie w wyznaczonych wczesniej odcinkach
    if flagi(ind) == 0  
        
        iL = ind; %indeksy lewy i prawy
        iP = ind;
        gL = 0;   %granice lewa i prawa
        gP = 0;
        
        %WYZNACZANIE SZEROKOSCI WZNIESIEN HISTOGRAMU
        while gL == 0 || gP == 0
            
            if gL == 0 iL = iL - 1; end
            if gP == 0 iP = iP + 1; end   
            
            %Jesli odcinek pokrywa sie z juz innym, wczesniej dodanym do
            %tablicy flagi[]
            if flagi(iL) == 1 || flagi(iP) == 1 
                gL = 0; 
                break;
            end
            
            if gL == 0 && s(iL) < prog
                gL = iL;
            end
            
            if gP == 0 && s(iP) < prog
                gP = iP;
            end 
        end
        
        if gL ~= 0;
            flagi(gL:gP) = 1;            
            hold all
            plot(gL:gP, prog*ones(1, gP-gL+1), 'black');
            
            wykr(gL:gP, int32(prog):int32(prog+7)) = 0;
        end        
    end    
end

subplot(8,2,12);
plot(flagi, 'black'), axis([0, n, 0, 2]);
% title([progIn sr],'FontSize',12);

granice = zeros(1, n);
nGranic = 0;
min = maksima(1)+1;
iMin = 0;
szer = -1;

%WYZNACZANIE GRANIC MIEDZY ZNALEZIONYMI ODCINKAMI
for i=1:n    
    if flagi(i) == 0    % szukamy minimum
        if s(i) < min
            min = s(i);
            iMin = i;
            szer = 0;
        
        else if s(i) == min
                szer = szer + 1;
             end
        end
    else if szer > -1
            nGranic = nGranic + 1;
            granice(nGranic) = iMin + szer/2;
            min = maksima(1)+1;
            szer = -1;
        end
    end
    
end

nGranic = nGranic + 1;
granice(1) = 1;
granice(nGranic) = n;


  %Wyswietlenie podzielonego obrazu
    B = A;
    for i=1:nGranic
            B(granice(i):(granice(i)+8), :) = 1;
            Ako(granice(i):(granice(i)+8), :, :) = 0;
            Ako(granice(i):(granice(i)+8), :, 1) = 255;
    end
%     figure(3), imshow(Ako);
    ObrazZLiniami = Ako;
    subplot(1, 2, 1); imshow(ObrazZLiniami);

    % 
    % B = B(1:n, :);
    % 
    % % tu bylo wykr...
    % 
    % B = [B, wykr];
    % 
    % figure(2); imshow(Ako);
    % % imshow(B);   % OBRAZ Z WYKRESEM WYSWIETLENIE
    % set(gcf,'position',get(0,'screensize'))

%figure(1);

%ZAMIANA GRANIC NA PRZEDZIALY
granice2 = granice(1:nGranic);
granice = zeros(nGranic - 1, 2);
for i=1:(nGranic-1)
    granice(i, 1:2) = [granice2(i) + 1, granice2(i+1)];
end
granice(1, 1) = 1;
nGranic = nGranic - 1;

end %function



function [granice nGranic] = wyznaczGraniceStalyProg(s, prog)

[n n2] = size(s);
n = int32(n);
prog = prog * max(s);

granice = zeros(1, n);
nGranic = 0;
iMin = 0;
stan = 0;

for i=1:n    
    if stan == 0
        if s(i) > prog
            stan = 1;
            nGranic = nGranic + 1;
            granice(nGranic) = (iMin + i)/2;
        end
    else
        if s(i) <= prog
            stan = 0;
            iMin = i;
        end
    end         
end

nGranic = nGranic + 1;
granice(nGranic) = (iMin + n)/2;
granice = granice(1:nGranic);

figure('units','normalized','outerposition',[0 0 1 1]), plot(s);
hold all
plot(granice, prog, 'r*');

granice2 = granice(1:nGranic);% 
granice = zeros(nGranic - 1, 2);% 
for i=1:(nGranic-1)
    granice(i, 1:2) = [granice2(i) + 1, granice2(i+1)];
end
nGranic = nGranic - 1;

end %function
