function [o2o_cnt, M, N] = matchscore1(labImg, inputPath)
  
  %% czytanie obrazow
  
  % Tru = getResultMat('ICDAR2013/005.tif','.dat');
  Tru = getResultMat(inputPath,'.dat');
  % Res = getResultMat('ICDAR2013/005.tif','.dat');
  Res = uint8(labImg);
  
  %% Liczenie statystyk
  
  prog=0.95;
  ind = Res>0 | Tru>0;
  [c, order]=confusionmat(Res(ind),Tru(ind));
  c(order==0,:)=[];
  c(:,order==0)=[];
  c(:,sum(c,1)==0)=[];
  c(sum(c,2)==0,:)=[];
  c1=sum(c,1);
  c2=sum(c,2);
  [M, N]=size(c);
  c_norm=c./(repmat(c1,[M, 1])+repmat(c2,[1, N])-c); %sprawdzic row i col (czy nie zamienic miejscami)
  match_count=(c_norm>prog);
  
  %% one to one
  
  g_proj = sum(match_count,1);
  r_proj = sum(match_count,2);
  o2o = match_count==1 & repmat(g_proj,[M,1])==1 & repmat(r_proj,[1,N])==1;
  o2o_cnt = sum(o2o(:));
  
  %% statystyki
  
  DR = o2o_cnt/N;
  
  RA = o2o_cnt/M;
  
  FM = 2*DR*RA/(DR+RA);
  
  %%zapisanie statystyk
  
end




%% functions

function M = getResultMat(name, suf)
  % GETRESULTMAT gets matrix of ground truth or result matrix from .dat file
  
  ImgInfo = imfinfo(name);
  fi = fopen([name, suf]);
  M = fread(fi,[ImgInfo.Width, ImgInfo.Height],'uint32=>uint8');
  M = M';
  fclose(fi);
end
