
%Wyznaczenie punktów krzywej łamanej
%DD - macierz przechowuje współrzędne y punktów stanowiących krzywe łamane
%pomiędzy punktami CC
%j - zlicza kolejne wyznaczone krzywe łamane
for j=1:size(CC,2)
    DD{j}=[];
    for i=1:size(CC{j},1)-1
        coef = polyfit([CC{j}(i,1), CC{j}(i+1,1)], [CC{j}(i,2), CC{j}(i+1,2)], 1);
        y = polyval(coef,[CC{j}(i,1):CC{j}(i+1,1)]);
        DD{j}=[DD{j}(1:end-1), round(y)];
    end
    plot(1:CC{j}(end,1),DD{j})
    hold on
end

im1=imread('/home/staff/faster/MATLAB/hwtv_isat19/ICDAR2013/005_1a.tif');
im2=1-im1;
%imshow(im2)
etykiety=bwlabel(im2);
% figure
% imshow(etykiety)
%imtool(etykiety)

% aa=DD{1}';
% bb=[1:1461]';
%EE - macierz z etykietami obszarów spójnych leżących na krzywych łamanych
%j - wyznacza zbiory etykiet związanych z kolejnymi krzywymi łamanymi
for j=1:size(DD,2)
  wspolrzedne_krzywej=[DD{j}' [1:size(DD{j},2)]'];
  etykiety_na_lini=[];
  for i=1:size(DD{j},2) %np. do 1461
    if etykiety(wspolrzedne_krzywej(i,1),wspolrzedne_krzywej(i,2)) > 0
         etykiety_na_lini=[etykiety_na_lini, etykiety(wspolrzedne_krzywej(i,1),wspolrzedne_krzywej(i,2))];
    end 
  end
  EE{j}=unique(etykiety_na_lini);
end


FF=zeros(size(etykiety));
for j=1:size(EE,2)
    for i=1:size(EE{1},2)
      FF(find(etykiety==EE{j}(i)))=j;
    end
end
imtool(FF)

%e1=[e1, etykiety(cc(i,1),cc(i,2))];

% e1=etykiety([cc(:,2), cc(:,1)])
% e2=etykiety([cc(:,2)]; [cc(:,1)]);
%e1=etykiety(DD{1}', [1:1461]');