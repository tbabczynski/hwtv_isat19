function ax = debugsubplot(x, y, nr)
%DEBUGSUBPLOT creates subplot in current figure or new figure

if false
  ax = subplot(x, y, nr);
else
  ax = axes(figure('Name', sprintf('%d,%d,%d',x,y,nr),'NumberTitle','off'));
end