function binarizedImage = binarizeImage(img,threshold) %#ok<INUSD>

if islogical(img) %already binary
    if median(img(:))== 0
        binarizedImage = img;
    else
        binarizedImage = ~img;
    end
    return;
end

binarizedImage = imbinarize(img,'adaptive', 'ForegroundPolarity','dark');
if median(binarizedImage(:))== 1
	binarizedImage = ~binarizedImage;
end
return;

% if nargin<2
%     threshold = graythresh(img)*intmax(class(img));
% end
% 
% [m,n] = size(img);
% binarizedImage=zeros(m,n,'logical');
% binarizedImage(img>=threshold)=1;
% for i = 1:numel(img)
%     if (img(i)>=threshold)
%         binarizedImage(i)=0;
%     end
% end
