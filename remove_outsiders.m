function [To] = remove_outsiders(T, AH)
  %REMOVE_OUTSIDERS removes tensors ???
  %   Removes tensors which do not pass an exam
  
  if ~(exist('AH','var') && isa(AH,'numeric'))
    AH = 10;
  end
  
   %% start
  [e1,e2,l1,l2] = convert_tensor_ev(T);
  ls = l1-l2;
  
  %% too round - nie ma u Chińczyków
  l2l1=l2./l1;
  ind = l2l1>prctile(l2l1(isfinite(l2l1)),90) | isnan(l2l1);
  %     ind1 = ind;
  %     l2(ind)=0;
  %     l1(ind)=0;
  
  %% too perpendicular
  %    ind = abs(e1(:,:,2))<cos(15*pi/180); %our
  ind = abs(e1(:,:,2))<cos(45*pi/180); %their
  %     ind2 = ind;
  l2(ind)=0;
  l1(ind)=0;
  
  %% too small
  %     ind = ls<prctile(ls(ls>0),10); %our
  ind = ls < 0.600 * mean(ls(ls>0)); %their 2
  %     ind3 = ind;
  l1(ind) = 0;
  l2(ind) = 0;
  
%   ind(:) = false;
  %% max in vertical
  [y,x] = find(ls);
  winWidth=floor(AH/4);
  for t=1:size(x)
    yf = max(1,y(t)-AH);
    yt = min(size(ls,1),y(t)+AH);
    xf = max(1,x(t)-winWidth);
    xt = min(size(ls,2),x(t)+winWidth);
    [oknoy, oknox]=find(ls(yf:yt,xf:xt));
    okno = sub2ind(size(ls),oknoy+yf-1, oknox+xf-1);
    if size(okno,1)>1
      [~,maxo]=max(ls(okno));
			ind_t = sub2ind(size(ls),y(t),x(t));
			if okno(maxo)~=ind_t
				ls(ind_t)=0;
				l1(ind_t)=0;
				l2(ind_t)=0;
%         ind(ind_t)=true;
			end
%       okno(maxo)=[];
%       ls(okno)=0;
%       l1(okno)=0;
%       l2(okno)=0;
    end
  end
  
  %% normalize
  l1max = max(l1(:));
  l1 = l1./l1max;
  l2 = l2./l1max;
  
  %% finally
  To=convert_tensor_ev(e1,e2,l1,l2);
  
  %     To = T;
  
end

