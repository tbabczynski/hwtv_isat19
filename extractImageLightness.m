function imgLightness = extractImageLightness(img)
if ischar(img)
    img = imread(img);
end

if size(img,3)==1
    imgLightness=img;
else

    imgLab = rgb2lab(img);
    imgLightness=imgLab(:,:,1);
    imgLightness = im2uint8(imgLightness./max(imgLightness(:)));
end
