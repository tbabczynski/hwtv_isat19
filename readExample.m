function [binImg,img] = readExample(inputPath,dbgLevel)
  %READEXAMPLE Summary of this function goes here
  %   Detailed explanation goes here
  % read the image, binarize and preprocess
  
  if nargin<2
    dbgLevel=0;
  end
  img = extractImageLightness(inputPath);
  
  % img = imresize(img,0.25);
  
  binImg = binarizeImage(img);
  
  if dbgLevel > 0
    debugsubplot(2,3,1); imshow(img);
    title('original image'); drawnow();
    debugsubplot(2,3,2); imshow(binImg);
    title('binary image');drawnow();
  end
end

