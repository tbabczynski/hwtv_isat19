classdef COptions  < handle
    %OPTIONS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        dense = false;
        sigma {mustBePositive} = 18.25;
        m_CrStickField = memoize(@create_stick_tensorfield);
    end
    
    methods
        function obj = COptions()
            %OPTIONS Construct an instance of this class
            %   Detailed explanation goes here
        end
        
%         function outputArg = set(obj,inputArg)
%             %METHOD1 Summary of this method goes here
%             %   Detailed explanation goes here
%             outputArg = obj.Property1 + inputArg;
%         end
    end
end

