function [ T, Tmax ] = calc_refined_field_full( tf, im, sigma, cached_tf )
%  CALC_REFINED_FIELD_FULL calculate the tensor field after tensor voting
%   each voter votes on each other
%
%   T = calc_refined_field( tf, im, sigma, cached_btf )
%
%   tf - voting tensors (n,m,2,2)
%   im - grayscale image
%   sigma - the range of voting parameter
%   cached_btf - OPTIONAL the cached voting field
%   
%   returns T - tensor field after voting
%   Tmax - tensor field after maximizing over vertical lines

  global options;

  if nargout > 1
    Tmax = [];
    if options.dense
      warning('for dense voting Tmax is not calculated');
    end
  end

  % Get votes
  if nargin < 4
    T = calc_vote_stick(tf,sigma);
  else
    T = calc_vote_stick(tf,sigma, cached_tf);
  end

  %% find max near voters

  if options.dense == false
    im1 = max_vertical(T, im, sigma/2);
    %     im1 = im;
    if nargout > 1
      Tmax = T;
    end
  end

  %% erase non-voters
  % Erase anything that's not in the original image
  if options.dense == false
    T = removeNonVoters(T, im);
%     im2 = max_vertical(T, im, sigma/2);
%     T = removeNonVoters(T, im2);
    if nargout >1
      Tmax = removeNonVoters(Tmax, im1);
    end
  end

end

%%functions

function im1 = max_vertical(T, im, neigh)
%MAX_VERTICAL find maximum on the vertical line going through each voter
%
%	T - tensor field
% im - image with original positions
% neigh - radius of neighborhood
%
% returns
%	im - binary image of maximal saliencies


	[my,mx] = size(im);
	
	[~,~,L1,L2] = convert_tensor_ev(T);
	LS = L1-L2;
	[row, col] = find(im);
	s = size(row,1);
	im1=zeros(size(im),'logical');

	for i=1:s
		loX = max(1,col(i)-neigh/2);
		hiX = min(mx,col(i)+neigh/2);
		loY = max(1,row(i)-neigh);
		hiY = min(my,row(i)+neigh);
		LStmp = LS(loY:hiY,loX:hiX);
		[~,mm] = max(LStmp(:));
    [mmY, mmX] = ind2sub(size(LStmp),mm);
		im1(loY-1+mmY,loX-1+mmX)=1;
	end

end

function T = removeNonVoters(T, im)
  [rows, cols] = find(im==0);
  s = size(rows,1);

  for i=1:s
    T(rows(i),cols(i),1,1) = 0;
    T(rows(i),cols(i),1,2) = 0;
    T(rows(i),cols(i),2,1) = 0;
    T(rows(i),cols(i),2,2) = 0;
%     T(rows(i),cols(i),:,:) = [0,0; 0,0]; % dziala duuuzo wolniej
  end
end