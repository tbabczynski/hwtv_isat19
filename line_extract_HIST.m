%% start
% definition of parameters

% clear variables;

dbgLevel = 1;
creategif=false;
% options.dense = true;
%inputDir='inputImg/'; %folder with input pictures
% inputFig='gandhis100.png'; %current picture
% inputFig='dokument007.jpg'; %current picture
% inputFig='005.tif'; %current picture
% inputFig='005_1a.tif'; %current picture
% inputFig='109.tif'; %current picture
% inputFig='254.tif'; %current picture
% inputFig='254a.tif'; %current picture
inputFig='169.tif'; %current picture
% inputFig='kreski.png'; %current picture

inputFigCell = inputdlg('Picture Name','Picture',1,{inputFig});
if isempty(inputFigCell)
  warning('Empty image list');
  return;
end
inputFig = inputFigCell{1};

DirInfo = dir(['**/', inputFig]);
inputPath = [DirInfo(1).folder, '/', inputFig];

%% read data
binImg = readExample(inputPath,dbgLevel);

%% start algorytmu

labImg = TextLineSepHist(binImg, dbgLevel);



