function [labels] = labelsFromLines(CC,img)
%LABELSFROMLINES Summary of this function goes here
%   Detailed explanation goes here

%Wyznaczenie punktów krzywej łamanej
%DD - macierz przechowuje współrzędne y punktów stanowiących krzywe łamane
%pomiędzy punktami CC
%j - zlicza kolejne wyznaczone krzywe łamane
DD = cell(size(CC));
for j=1:size(CC,2)
	for i=1:size(CC{j},1)-1
		coef = polyfit([CC{j}(i,1), CC{j}(i+1,1)], [CC{j}(i,2), CC{j}(i+1,2)], 1);
		y = round(polyval(coef,CC{j}(i,1):CC{j}(i+1,1)));
    y(y<1) = 1; %czasem z zaokrąglenia wychodzą piksel poza obrazkiem
    y(y>size(img,1))=size(img,1);
		DD{j}=[DD{j}(1:end-1), y];
	end
% 	plot(1:CC{j}(end,1),DD{j})
% 	hold on
end

%imshow(img)
etykiety=bwlabel(img);
% figure
% imshow(etykiety)
%imtool(etykiety)

%EE - macierz z etykietami obszarów spójnych leżących na krzywych łamanych
%j - wyznacza zbiory etykiet związanych z kolejnymi krzywymi łamanymi
EE = cell(size(DD));
for j=1:size(DD,2)
	wspolrzedne_krzywej=[DD{j}' [1:size(DD{j},2)]'];
	etykiety_na_lini=[];
	for i=1:size(DD{j},2) %np. do 1461
		if etykiety(wspolrzedne_krzywej(i,1),wspolrzedne_krzywej(i,2)) > 0
			etykiety_na_lini=[etykiety_na_lini, etykiety(wspolrzedne_krzywej(i,1),wspolrzedne_krzywej(i,2))];
		end
	end
	EE{j}=unique(etykiety_na_lini);
  
end


labels=zeros(size(etykiety));
for j=1:size(EE,2)
	for i=1:size(EE{j},2)
		labels(etykiety==EE{j}(i))=j;
	end
end
% imtool(labels)

%% rysowanie po obrazku
  

end

