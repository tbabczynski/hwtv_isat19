%% czytanie obrazow
Img = imread('ICDAR2013/005_1a.tif');
fi = fopen('ICDAR2013/005.tif.dat');
%Tru = fread(fi,flip(size(Img)),'uint32=>uint32')';
fclose(fi);

%% skalowanie
ImgS = imresize(Img,0.5,'bilinear');

%% dylatacja

se=strel('line',16,0);
sep = strel('sphere',1);

ImgS2 = imdilate(Img,sep);

ImgDilS = imerode(ImgS,se);

%% pokaz

figure;
subplot(2,1,1); 
imshow(Img);
%subplot(2,1,2); 
%figure;
%imshow(ImgDil);
